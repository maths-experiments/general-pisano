# General Pisano

The ultimate aim of this script is to calculate periodic Fibonacci sequences as fast as possible.

Fibonacci sequence is periodic if you take the modulus of the members. This period is called Pisano period.
You can read about it here: https://en.wikipedia.org/wiki/Pisano_period

My script is generalizing this concept in several ways. 
The main focus is on the different sequences and their periods yielded when we set an arbitrary base and number of terms.

## Example 
Usually we have initial terms [0, 1] when we talk about a Fibonacci sequence or [2, 1] are the initial terms of Lucas Sequence
In this script I'm working on to find all unique sequences with b^m (b different digits to the power of m initial terms) different starting sequences.
As the complexity is exponential optimisation is important.

Let's call this entity a Pisano System PS(base,members,rule) or short PS(b,m,rule) which contains all the unique sequences generated by all possible initial terms.
	b:= the number base or modulo of the residue class of Fibonacci number
	m:= the number of terms to be added
	rule:= the vector of multipliers of the terms. by default it is [1,1], in this case we just add the previous two terms.
	
When b=10, m=2, rule = [1,1] we're interested in the last digits of the Fibonacci numbers in base 10 and the other 99 sequences nobody talks about.
	Calculating all possible initial sequences we get 6 unique sequences with differenc period lenghts: 1,3,4,12,20,60
	The smallest initial sequences and their period lengths:
	[0,0]	1
	[0,5]	3
	[2,6]	4
	[1,3]	12
	[0,2]	20
	[0,1]	60
	
	I could have also written [1,1]->60 or [2,1]->20 as these initial sequences are part of the longer sequences.
	An example for calculating the last digits of the modified 'Fibonacci' sequence with initials [1,3] in base 10:
		[1,3] -> [1,3,4,7,1,8,9,7,6,3,9,2,1,3]
	With the last two members we reached the initials, so the period of the sequence is 12. 
	We would get the same period if we'd choose any two-long subsequence, for example [2,1] which are the initial sequence of the Lucas sequence.
	So we got 6 unique sequences and their periods add up to 100. That's exactly b^m, 10^2 because from [0,0] to [9,9] we have exactly 10^2 different subsequences.

## Running the script

  --path TEXT  The path of the output folder.

  --b INTEGER  The number base. Each number of the sequence taken modulo b. [required]
  
  --m INTEGER  The last m members of the sequence to be summed to get the next one. [required]

	example:
	python general_pisano.py --b 10 --m 2
	
## Algorithms
	to be updated

## To be implemented:
	Algorithms:
		speed up period calculation:
			modular exponention
	Pandas:
		Storing the results in pandas dataframe
		save and load it to be able to use plots
	Fix:
		get Pisano Systems for arbitrary rules
	Optimize:
		direction of iteration

## Prerequisites

```
Python 3.8.5
click 7.1.1
numpy 1.19.1
pycodestyle 2.6.0
```

## Installing

```
pip install click
pip install numpy
pip install pycodestyle

or

pip install -r requirements.txt
```

## License
MIT