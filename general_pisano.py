import time
import os
from random import *
import math
import itertools
import psutil
import datetime
import click
import numpy as np
import timeit
from sampler import Sampler


log_path = ''
log_file = ''
global_cnt = 0


def set_paths(path):

    global log_file
    global log_path

    log_path = path
    log_file = 'general_pisano.log'


def increment_seq(seq, base, power=0):
    condition = True
    # which place to increment in the seq
    index = -power - 1

    while condition and len(seq) >= -index:
        current_digit = seq[index]
        incr_digit = (current_digit + 1) % base
        condition = incr_digit < current_digit
        seq[index] = incr_digit % base
        if condition:
            index = index - 1
    # print(seq)
    return seq


def decrement_seq(seq, base, power=0):
    condition = True
    # which place to increment in the seq
    index = -power - 1

    while condition and len(seq) >= -index:
        current_digit = seq[index]
        decr_digit = (current_digit - 1) % base
        condition = decr_digit > current_digit
        seq[index] = decr_digit
        if condition:
            index = index - 1
    # print(seq)
    return seq


def set_current_seq(bottom_seq, top_seq, top_bottom_cnt, seq_store_dict, base):

    if top_bottom_cnt:
        return get_greatest_unseen_seq(bottom_seq, top_seq, seq_store_dict, base)
    else:
        return get_smallest_unseen_seq(bottom_seq, top_seq, seq_store_dict, base)


def get_smallest_unseen_seq(bottom_seq, top_seq, seq_store_dict, base):
    seq_already_in_store = True
    delete_seq_from_store(bottom_seq, seq_store_dict)
    while seq_already_in_store and (bottom_seq < top_seq or all([v == 0 for v in top_seq])):
        current_seq = tuple(increment_seq(bottom_seq, base))
        seq_already_in_store = current_seq in seq_store_dict
        if seq_already_in_store:
            del seq_store_dict[current_seq]
        else:
            return list(current_seq)
    if bottom_seq == top_seq and sum(top_seq) == 0:
        return increment_seq(bottom_seq, base)
    elif bottom_seq == top_seq:
        return bottom_seq


def get_greatest_unseen_seq(bottom_seq, top_seq, seq_store_dict, base):
    seq_already_in_store = True
    delete_seq_from_store(top_seq, seq_store_dict)
    while seq_already_in_store and (bottom_seq < top_seq or all([v == 0 for v in top_seq])):
        # print('ggus', top_seq)
        current_seq = tuple(decrement_seq(top_seq, base))
        # print(current_seq)
        seq_already_in_store = current_seq in seq_store_dict
        if seq_already_in_store:
            del seq_store_dict[current_seq]
        else:

            return list(current_seq)
    if bottom_seq == top_seq and sum(top_seq) == 0:
        return decrement_seq(top_seq, base)
    elif bottom_seq == top_seq:
        return top_seq


def delete_seq_from_store(seq, seq_store_dict):
    seq_tuple = tuple(seq)
    if seq_tuple in seq_store_dict:
        del seq_store_dict[seq_tuple]


def mod_fib_periods(base, members, rule, start_time, sampler, complexity):
    """

    """
    seq_store_dict = {}
    pisano_partition = {}
    interesting_sequences_dict = {}

    current_period_sum = 0
    global_initial_seq = [0] * members
    bottom_seq = global_initial_seq[:]
    top_seq = decrement_seq(bottom_seq[:], base)
    current_seq = None

    seq_sum = [np.uint64(0)]
    all_seq_found = False
    seq_cnt = 0
    top_bottom_cnt = 0

    if all(val == 1 for val in rule):
        rule = None

    while not all_seq_found and not top_seq == bottom_seq:
        if current_seq is None:
            current_seq = bottom_seq

        elif current_seq == global_initial_seq:
            current_seq = top_seq

        else:
            if top_bottom_cnt:

                top_seq = set_current_seq(bottom_seq, top_seq, top_bottom_cnt, seq_store_dict, base)
                # print(top_seq)
                current_seq = top_seq[:]
            else:
                bottom_seq = set_current_seq(bottom_seq, top_seq, top_bottom_cnt, seq_store_dict, base)
                current_seq = bottom_seq[:]

        sampling_frequency = sampler.get_sampling_frequency()
        # print('newinitseq', current_seq, 'b', bottom_seq, 't', top_seq)
        get_sequence(current_seq, base, members, rule, sampling_frequency, pisano_partition, seq_sum, top_bottom_cnt, interesting_sequences_dict, seq_store_dict, bottom_seq, top_seq, verbose=False)

        seq_cnt += 1
        top_bottom_cnt = seq_cnt % 2

        all_seq_found = complexity <= seq_sum[0]

        sampler.update_sampler(seq_cnt)

    # print ('store length', len(seq_store_dict))
    # check_seq_store_dict(seq_store_dict, bottom_seq, top_seq, seq_cnt)

    return pisano_partition, interesting_sequences_dict, seq_store_dict


def check_seq_store_dict(seq_store_dict, bottom_seq, top_seq, seq_cnt):
    print('bottom', bottom_seq)
    print('top', top_seq)
    print('store length', len(seq_store_dict))
    print('sequences started', seq_cnt)
    # print('illegal store entries:', count_illegal_store_entries(seq_store_dict, bottom_seq, top_seq))


def count_illegal_store_entries(seq_store_dict, bottom_seq, top_seq):
    cnt = 0
    for k in seq_store_dict:
        k = list(k)
        if k < bottom_seq or k > top_seq:
            # print('illegal seq', k)
            cnt += 1
            # print(k)
    return cnt


def iterate_sequence(seq, base, rule=None):
    new_member = mult_vector(seq, rule) % base
    seq.pop(0)
    seq.append(new_member)
    return seq


def count_digit(digit, digit_cntr):
    digit_cntr[digit] += 1


def mult_vector(seq, rule):

    if rule is None:
        return sum(seq)
    else:
        # return sum([(x * y) for x, y in  itertools.zip_longest(seq, rule)])
        return np.dot(seq, rule)


def get_sequence(initial_seq, base, members, rule, sampling_frequency=None, pisano_partition=None, seq_sum=None, direction_top=None, interesting_sequences_dict=None, seq_store_dict=None, bottom_seq=None, top_seq=None, verbose=False):
    """
        generate sequence for the given sequence starting *initial_seq*
    """
    global global_cnt

    if len(initial_seq) == members:

        current_seq = initial_seq[:]

        reached_initial_seq = False
        seq_already_seen = False

        period_cnt = 0

        # to do: only count digits when needed
        digit_cntr = [0] * base

        while not reached_initial_seq and not seq_already_seen:
            """
                runs until the last *members*-long substring in *current_seq* will be the same as the starting sequence
            """
            # print(current_seq, 'top', top_seq, 'bottom', bottom_seq)
            # print(current_seq)
            current_seq = iterate_sequence(current_seq, base, rule)

            digit_cntr[current_seq[-1]] += 1
            # last_digit = current_seq[-1]
            # count_digit(last_digit, digit_cntr)
            # print(current_seq)

            reached_initial_seq = current_seq == initial_seq

            if bottom_seq is not None and top_seq is not None:
                seq_already_seen = seq_seen(current_seq, bottom_seq, top_seq, seq_store_dict, direction_top)

                if not seq_already_seen and sampling_frequency is not None:
                    store_seq(current_seq, bottom_seq, top_seq, seq_store_dict, sampling_frequency)

            period_cnt += 1
            global_cnt += 1

            if verbose:
                print(current_seq)

        if reached_initial_seq and not seq_already_seen:
            # print('new found!', initial_seq, period_cnt)
            if seq_sum is not None:
                seq_sum[0] += period_cnt

            if pisano_partition is not None:
                if period_cnt in pisano_partition:
                    pisano_partition[period_cnt] += 1
                else:
                    pisano_partition[period_cnt] = 1
                    if interesting_sequences_dict is not None:
                        interesting_sequences_dict[tuple(initial_seq)] = [period_cnt, digit_cntr]
            else:
                # print('period length of sequence', period_cnt)
                # print('digit counter', sorted(digit_cntr.items(), key=lambda item: item[0]))
                return period_cnt

        if verbose:
            print("sequence: " + str(current_seq))

    else:
        print("given sequence-length is not matching with the number of members")


def seq_seen(current_seq, bottom_seq, top_seq, seq_store_dict, direction_top):
    # current_seq = list(current_seq)
    if direction_top:
        # print(current_seq, '<=', top_seq, current_seq <= top_seq)
        if current_seq <= bottom_seq or current_seq > top_seq:
            return True
    else:
        if current_seq < bottom_seq or current_seq >= top_seq:
            return True

    if tuple(current_seq) in seq_store_dict:
        # print("seq already in store", current_seq)
        return True

    # when none of the above conditions are true
    return False


def store_seq(current_seq, bottom_seq, top_seq, seq_store_dict, sampling_frequency):

    # store seq if it's chosen by sampling
    try:
        if random() <= sampling_frequency:
            seq_store_dict[tuple(current_seq)] = True
    except:
        # in case of memory overflow do nothing
        # i don't think this is ok
        pass


def save_and_print_log(base, members, rule, pisano_partition, interesting_sequences_dict, seq_store, sampling_frequency, complexity, start_time, only_print, only_save):

    if only_print and only_save:
        raise ValueError("only_print and only_save can't be True at the same time")

    if only_print:
        log_results(base, members, rule, pisano_partition, interesting_sequences_dict, seq_store, sampling_frequency, complexity, start_time, file=None)
        print('\n\n')

    else:
        with open(log_path + log_file, 'a') as file_w:
            file_w.write('\n\n')
            file_w.write(os.path.basename(__file__) + '\n')
            log_results(base, members, rule, pisano_partition, interesting_sequences_dict, seq_store, sampling_frequency, complexity, start_time, file_w, only_save)
            if not only_save:
                print('\n\n')


def log_line(line_tuple, log_file, only_save):

    if isinstance(line_tuple, tuple):
        if not only_save:
            print(*line_tuple)
        new_line = ''.join((str(k) + '\t' for k in line_tuple)).strip() + '\n'
    else:
        if not only_save:
            print(line_tuple)
        new_line = str(line_tuple) + '\n'

    if log_file:
        log_file.write(new_line)


def log_results(base, members, rule, pisano_partition, interesting_sequences_dict, seq_store, sampler, complexity, start_time, file, only_save):

    end_time = time.perf_counter()
    elapsed_time = end_time - start_time

    log_line(('base:', base, 'members:', members), file, only_save)
    log_line(('rule:', rule), file, only_save)

    log_line(('-----check result-----'), file, only_save)
    seq_sum = sum(int(k) * v for k, v in pisano_partition.items())
    if complexity != seq_sum:
        log_line(('TROUBLE - the sum of periods is NOT OK'), file, only_save)
        log_line(('seqs sum\t', seq_sum), file, only_save)
    else:
        log_line(('the sum of periods is OK'), file, only_save)
    log_line(('complexity\t', np.uint64(complexity)), file, only_save)

    log_line(('-----period counts-----'), file, only_save)
    for k in sorted(pisano_partition.keys(), key=lambda x: int(x)):
        log_line(('count:', pisano_partition[k], 'length:', k), file, only_save)

    log_line(('-----example sequences-----'), file, only_save)
    for k, v in sorted(interesting_sequences_dict.items(), key=lambda item: item[1]):
        digit_dict = {i: v[1][i] for i in range(len(v[1])) if v[1][i] != 0}
        log_line((k, v[0], digit_dict), file, only_save)

    log_line(('-----stats-----'), file, only_save)
    log_line(('unique sequences:', sum(pisano_partition.values())), file, only_save)
    log_line(('unique periods:', len(pisano_partition)), file, only_save)
    log_line(('longest period:', max(int(k) for k in pisano_partition.keys())), file, only_save)
    log_line(('frequency of the most freq. period:', max(int(k) for k in pisano_partition.values())), file, only_save)
    log_line(('unique period frequencies:', len(set(int(k) for k in pisano_partition.values()))), file, only_save)

    log_line(('-----script stats-----'), file, only_save)
    log_line(('sampling frequency:', '{0:<8.5f}'.format(sampler.get_sampling_frequency())), file, only_save)
    log_line(('all steps taken:', global_cnt), file, only_save)
    log_line(('steps taken on avg:', '{0:<8.2f}'.format(global_cnt / complexity)), file, only_save)

    process = psutil.Process(os.getpid())
    memory_used = process.memory_info().rss
    log_line(('memory usage in bytes:', memory_used), file, only_save)

    now = datetime.datetime.now()
    log_line(('script finished at:', now.strftime("%Y-%m-%d %H:%M")), file, only_save)

    st_per_complexity = elapsed_time * int(memory_used) / float(complexity)
    log_line(('space-time efficiency per unit complexity:', '{0:<8.2f}'.format(st_per_complexity)), file, only_save)
    log_line(('computation time:', '{0:<8.5f} sec'.format(elapsed_time)), file, only_save)


def get_period_of_seq(seq, base, rule=None):
    '''The simple version of get_sequence'''
    members = len(seq)
    return get_sequence(seq, base, members, rule, sampling_frequency=None, pisano_partition=None, seq_sum=None, direction_top=None, interesting_sequences_dict=None, seq_store_dict=None, bottom_seq=None, top_seq=None, verbose=False)


@click.command()
@click.option('--path', default='', help='The path of the output folder.')
@click.option('--b', required=True, type=int, help='The number base. Each number of the sequence taken modulo b.')
@click.option('--m', required=True, type=int, help='The last m members of the sequence to be summed to get the next one.')
def main(path, b, m):

    base = b
    members = m
    set_paths(path)
    complexity = np.power(np.uint64(base), members)

    rule = np.ones((members,), dtype=int)

    start_time = time.perf_counter()
    complexity = np.power(np.uint64(base), members)

    sampler = Sampler(complexity, 1000, update=True)
    # sampler = Sampler(complexity,0, update=False)
    pisano_partition, interesting_sequences_dict, seq_store = mod_fib_periods(base, members, rule, start_time, sampler, complexity)
    save_and_print_log(base, members, rule, pisano_partition, interesting_sequences_dict, seq_store, sampler, complexity, start_time, only_print=False, only_save=False)


if __name__ == "__main__":
    main()