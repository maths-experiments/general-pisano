import numpy as np
import psutil
import os


class Sampler():
    '''
    Sampler class is regulating the sampling frequency of the sequences: the sampled sequences become a part of the store of seen sequences
    its aim is to quciken the algorithm while preventing memory overflow
    The idea of sampling the sequences came from the birthday paradox,
    it's enough to have a relatively small portion of the whole population in order to have a high chance that they have matching properties in this case: already seen
    it's not at all following the logic of the paradox: the paradox describes a small portion (23 randomly choosen from 365) of all possible outcome (365)
    this case: from b^m outcome we choose sampling_freq*b^m
    '''

    def __init__(self, complexity, sampling_factor=None, update=False, mem_treshold=None, mem_extreme=None, increase_rate=None):
        self.complexity = complexity
        self.sqrt_complexity = np.power(complexity, 0.5)

        if sampling_factor is not None:
            self.sampling_factor = np.uint64(sampling_factor)
        else:
            self.sampling_factor = np.uint64(200)

        if increase_rate is not None:
            self.increase_rate = increase_rate
        else:
            self.increase_rate = 1.1

        if mem_treshold is not None:
            self.mem_treshold = mem_treshold
        else:
            self.mem_treshold = 4 * 1024 * 1024 * 1024

        if mem_extreme is not None:
            self.mem_extreme = mem_extreme
        else:
            self.mem_extreme = 5 * 1024 * 1024 * 1024

        self.update = update
        self.sampler_update_interval = int(np.power(complexity, 1.0 / 5))
        self.sampling_frequency = float(self.sampling_factor) / self.sqrt_complexity


    def get_sampling_factor(self):
        return self.sampling_factor


    def set_sampling_factor(self, sf):
        self.sampling_factor = sf
        self.update_sampling_frequency()


    def increase_sampling_factor(self, increase_rate=None):
        if self.sampling_frequency <= 1:
            if increase_rate is None:
                self.sampling_factor *= self.increase_rate
            else:
                self.sampling_factor *= increase_rate
            self.update_sampling_frequency()


    def decrease_sampling_factor(self, decrease_rate=None):
        if decrease_rate is None:
            self.sampling_factor /= self.increase_rate
        else:
            self.sampling_factor *= decrease_rate
        self.update_sampling_frequency()


    def update_sampling_frequency(self):
        self.sampling_frequency = float(self.sampling_factor) / self.sqrt_complexity


    def get_sampling_frequency(self):
        return self.sampling_frequency


    def update_sampler(self, counter):
        if self.update:
            if counter % self.sampler_update_interval == 1:
                process = psutil.Process(os.getpid())
                memory_used = process.memory_info().rss
                if memory_used >= np.uint64(self.mem_extreme):
                    self.set_sampling_factor(0)
                elif memory_used >= np.uint64(self.mem_treshold):
                    self.decrease_sampling_factor(0.50)
                else:
                    if self.sampling_factor > 0:
                        self.increase_sampling_factor(1.05)
                    else:
                        self.set_sampling_factor(1)