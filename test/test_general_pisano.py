import general_pisano as gp
import numpy as np
import time
import unittest
import random


class TestSeq(unittest.TestCase):

    def test_increment01(self):

        seq = [0, 0, 0, 0]
        base = 2
        power = 0

        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 0, 1], "Should be equal")

        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 1, 0], "Should be equal")

        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 1, 1], "Should be equal")

        seq = [1, 1, 1, 1]
        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 0, 0], "Should be equal")


    def test_increment02(self):

        seq = [0, 0, 0]
        base = 3
        power = 1

        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 1, 0], "Should be equal")

        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 2, 0], "Should be equal")

        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [1, 0, 0], "Should be equal")

        seq = [2, 2, 2]
        power = 0
        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 0], "Should be equal")

        seq = [2, 2, 2]
        power = 1
        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 2], "Should be equal")

        seq = [2, 2, 2]
        power = 2
        gp.increment_seq(seq, base, power)
        self.assertEqual(seq, [0, 2, 2], "Should be equal")


    def test_increment03(self):
        '''
        creating passing a new list to increment_seq will leave the original untouched
        '''
        seq = [0, 0, 0, 0]
        base = 2
        power = 0

        gp.increment_seq(seq[:], base, power)
        self.assertEqual(seq, [0, 0, 0, 0], "Should be equal")

        seq2 = gp.increment_seq(seq[:], base, power)
        self.assertEqual(seq2, [0, 0, 0, 1], "Should be equal")

        power = 2
        seq3 = gp.increment_seq(seq[:], base, power)
        self.assertEqual(seq3, [0, 1, 0, 0], "Should be equal")


    def test_decrement01(self):

        seq = [0, 0, 0, 0]
        base = 2
        power = 0

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [1, 1, 1, 1], "Should be equal")

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [1, 1, 1, 0], "Should be equal")

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [1, 1, 0, 1], "Should be equal")

        seq = [0, 0, 0, 1]
        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [0, 0, 0, 0], "Should be equal")


    def test_decrement02(self):

        seq = [0, 0, 0]
        base = 3
        power = 1

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [2, 2, 0], "Should be equal")

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [2, 1, 0], "Should be equal")

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [2, 0, 0], "Should be equal")

        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [1, 2, 0], "Should be equal")

        seq = [2, 2, 2]
        power = 0
        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [2, 2, 1], "Should be equal")

        seq = [2, 2, 2]
        power = 1
        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [2, 1, 2], "Should be equal")

        seq = [2, 2, 2]
        power = 2
        gp.decrement_seq(seq, base, power)
        self.assertEqual(seq, [1, 2, 2], "Should be equal")


    def test_decrement03(self):
        '''
        passing a new list to decrement_seq will leave the original untouched
        '''
        seq = [0, 0, 0, 0]
        base = 2
        power = 0
        gp.decrement_seq(seq[:], base, power)
        self.assertEqual(seq, [0, 0, 0, 0], "Should be equal")

        seq2 = gp.decrement_seq(seq[:], base, power)
        self.assertEqual(seq2, [1, 1, 1, 1], "Should be equal")

        power = 2
        seq3 = gp.decrement_seq(seq[:], base, power)
        self.assertEqual(seq3, [1, 1, 0, 0], "Should be equal")


    def test_mult_vector(self):
        seq = [3, 1]
        rule = [1, 1]
        self.assertEqual(gp.mult_vector(seq, rule), 4)
        seq = [3, 1]
        rule = [1, 2]
        self.assertEqual(gp.mult_vector(seq, rule), 5)
        seq = [3, 1]
        rule = [2, 1]
        self.assertEqual(gp.mult_vector(seq, rule), 7)


    def test_iterate_seq01(self):

        base = 3
        seq = [1, 2, 0]
        members = len(seq)
        rule = [1] * members
        gp.iterate_sequence(seq, base, rule)
        result_seq = seq
        self.assertEqual(result_seq, [2, 0, 0], "Should be equal")
        gp.iterate_sequence(result_seq, base, rule)
        self.assertEqual(result_seq, [0, 0, 2], "Should be equal")
        gp.iterate_sequence(result_seq, base, rule)
        self.assertEqual(result_seq, [0, 2, 2], "Should be equal")
        gp.iterate_sequence(result_seq, base, rule)
        self.assertEqual(result_seq, [2, 2, 1], "Should be equal")


    def test_iterate_seq02(self):

        base = 3
        seq = [1, 2, 0]
        members = len(seq)
        rule = [1, 2, 3]
        gp.iterate_sequence(seq, base, rule)
        result_seq = seq
        self.assertEqual(result_seq, [2, 0, 2], "Should be equal")
        gp.iterate_sequence(result_seq, base, rule)
        self.assertEqual(result_seq, [0, 2, 2], "Should be equal")
        gp.iterate_sequence(result_seq, base, rule)
        self.assertEqual(result_seq, [2, 2, 1], "Should be equal")
        gp.iterate_sequence(result_seq, base, rule)
        self.assertEqual(result_seq, [2, 1, 0], "Should be equal")


class TestSeqPeriod(unittest.TestCase):

    def test_seq_period01(self):

        seq = [0, 1]
        base = 5

        self.assertEqual(gp.get_period_of_seq(seq, base), 20, "Should be equal")
        base = 6
        self.assertEqual(gp.get_period_of_seq(seq, base), 24, "Should be equal")
        base = 7
        self.assertEqual(gp.get_period_of_seq(seq, base), 16, "Should be equal")
        base = 8
        self.assertEqual(gp.get_period_of_seq(seq, base), 12, "Should be equal")
        base = 9
        self.assertEqual(gp.get_period_of_seq(seq, base), 24, "Should be equal")
        base = 10
        self.assertEqual(gp.get_period_of_seq(seq, base), 60, "Should be equal")


    def test_seq_period02(self):

        seq = [0, 0, 1]
        members = len(seq)
        base = 5
        self.assertEqual(gp.get_period_of_seq(seq, base), 31, "Should be equal")
        base = 6
        self.assertEqual(gp.get_period_of_seq(seq, base), 52, "Should be equal")
        base = 7
        self.assertEqual(gp.get_period_of_seq(seq, base), 48, "Should be equal")
        base = 8
        self.assertEqual(gp.get_period_of_seq(seq, base), 16, "Should be equal")
        base = 9
        self.assertEqual(gp.get_period_of_seq(seq, base), 39, "Should be equal")
        base = 10
        self.assertEqual(gp.get_period_of_seq(seq, base), 124, "Should be equal")


    def test_seq_period03(self):

        seq = [4, 2]
        members = len(seq)
        base = 10
        self.assertEqual(gp.get_period_of_seq(seq, base), 4, "Should be equal")


if __name__ == "__main__":
    unittest.main()