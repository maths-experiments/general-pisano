from sampler import Sampler
import numpy as np
import unittest


class TestSampler(unittest.TestCase):

    def mock_update_sampler(self, sampler, cnt, mock_memory):

        memory_used = mock_memory

        if memory_used >= np.uint64(sampler.mem_extreme):
            sampler.set_sampling_factor(0)
        elif memory_used >= np.uint64(sampler.mem_treshold):
            sampler.decrease_sampling_factor(0.95)
        else:
            if sampler.sampling_factor > 0:
                sampler.increase_sampling_factor(1.05)
            else:
                sampler.set_sampling_factor(0.2)


    def test_Sampler(self):

        base = 3
        members = 20
        complexity = np.power(np.uint64(base), members)

        sampler = Sampler(complexity, sampling_factor=200, update=True, mem_treshold=5368709120, mem_extreme=7516192768, increase_rate=1.1)
        sampling_frequency1 = sampler.get_sampling_frequency()

        # case1: the memory usage is below the treshold so the sampling frequency should increase
        self.mock_update_sampler(sampler, 1, mock_memory=5368709119)
        sampling_frequency2 = sampler.get_sampling_frequency()
        self.assertTrue(sampling_frequency2 > sampling_frequency1)

        # case2: the memory usage is above the treshold so the sampling frequency should decrease
        self.mock_update_sampler(sampler, 1, mock_memory=5568709119)
        sampling_frequency3 = sampler.get_sampling_frequency()
        self.assertTrue(sampling_frequency2 > sampling_frequency3)

        # case3: the memory usage is above the extreme treshold so the sampling frequency should be set to 0
        self.mock_update_sampler(sampler, 1, mock_memory=8568709119)
        sampling_frequency4 = sampler.get_sampling_frequency()
        self.assertTrue(sampler.get_sampling_factor() == 0)

        # case4: the memory usage is below the treshold so the sampling frequency should be again greater than 0
        self.mock_update_sampler(sampler, 1, mock_memory=3568709119)
        sampling_frequency5 = sampler.get_sampling_frequency()
        self.assertTrue(sampler.get_sampling_factor() > 0)


if __name__ == "__main__":
    unittest.main()
